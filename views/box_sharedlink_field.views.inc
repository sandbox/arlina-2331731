<?php

/**
 * @file
 * Views integration for box_sharedlink_field module.
 */

/**
 * Implements hook_field_views_data_alter().
 */
function box_sharedlink_field_field_views_data_alter(&$result, $field, $module) {
  // Expose the following fields to Views as "field" => "handler".
  $columns = array(
    'name' => 'field',
    'last_fetched' => 'date',
    'created_at' => 'date',
    'modified_at' => 'date',
    'metadata' => 'serialized',
  );

  if ($field['type'] == 'box_sharedlink') {
    $fieldname = $field['field_name'];
    foreach ($result as $table => $definition) {
      if (isset($result[$table][$fieldname]['field'])) {
        foreach ($columns as $column => $handler) {
          $result[$table][$fieldname . '_' . $column]['argument']['handler'] = 'views_handler_argument_' . $handler;
          $result[$table][$fieldname . '_' . $column]['filter']['handler'] = 'views_handler_filter_' . $handler;
          $result[$table][$fieldname . '_' . $column]['sort']['handler'] = 'views_handler_sort_' . $handler;
          $result[$table][$fieldname . '_' . $column]['field'] = $result[$table][$fieldname]['field'];
          $result[$table][$fieldname . '_' . $column]['field']['handler'] = 'views_handler_field_' . $handler;
          $result[$table][$fieldname . '_' . $column]['field']['field_name'] = $fieldname . '_' . $column;
          $result[$table][$fieldname . '_' . $column]['field']['real field'] = $fieldname . '_' . $column;
        }
      }
    }
  }
}

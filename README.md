Box Shared Link Field
=====================

Provides a field that stores a Box sharing link, and displays metadata information about the file. Available metadata fields:

* File name
* File size
* File description
* Created at
* Modified at


### Setup:

1. Create an application on Box, and take note of the Client ID and Secret.
2. Add the following line to your `settings.php` to be able to authorize your app:
`$conf['https'] = TRUE;`
2. Download module, ensuring dependencies, and enable.
3. You can now add fields of type "Box Shared Link" to your entities.
4. When adding the fields, first save the Client ID and Secret on the field settings page, then come back and click the "Authorize" button.
5. Configure the "Display" settings for the field. Options are give as to which fields from the metadata should be displayed.


### Dependencies:

* Box API: https://www.drupal.org/sandbox/arlina/box_api
